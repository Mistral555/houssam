/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.junit_test.Calculatrice;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.assertj.core.api.Assertions.*;

/**
 *
 * @author mistral
 */
public class CalculatriceTest {
    
    
    static Long startTestGlobal;
    static Long endTestGlobal;
    
    Long startTest;
    Long endTest;
    Calculatrice calc;
    
    public CalculatriceTest() {
        calc = new Calculatrice();
    }
    
    @BeforeAll
    public static void setUpClass() {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Début des test de Calcultarice!\n");
        startTestGlobal = System.currentTimeMillis();
    }
    
    @AfterAll
    public static void tearDownClass() {
        endTestGlobal = System.currentTimeMillis();
        Long duree = endTestGlobal - startTestGlobal;
        System.out.println("Fin des test de Calculatrice en " + duree + " milliseconde!");
        System.out.println("--------------------------------------------------------------\n");
        
    }
    
    @BeforeEach
    public void setUp() {
        startTest = System.currentTimeMillis();
        System.out.println("Début test");
    }
    
    @AfterEach
    public void tearDown() {
        endTest = System.currentTimeMillis();
        Long duree = endTest - startTest;
        System.out.println("Fin du test en " + duree + " milliseconde!\n");
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @DisplayName("Test Caclulatrice.addition()")
    @Test
    void testAddition() {
        assertThat(calc.addition(2, 3)).isEqualTo(5);
        assertThat(calc.addition(2, -3)).isEqualTo(-1);
        assertThat(calc.addition(1, Integer.MAX_VALUE - 1)).isEqualByComparingTo(Integer.MAX_VALUE);
    }
     
    @DisplayName("Test Calculatrice.soustraction()")
    @Test
    void testSoustraction() {
        assertEquals(-2, calc.soustraction(1, 3));
        assertEquals(2, calc.soustraction(3, 1));
        assertEquals(4, calc.soustraction(1, -3));
    }
    
    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    void testMultiplication() {
        assertThat(calc.multiplication(1, 3)).isEqualTo(3);
        assertThat(calc.multiplication(-1, 3)).isEqualTo(-3);
        assertThat(calc.multiplication(1, 0)).isEqualTo(0);
    }
    
    @DisplayName("Test Calculatrice.division()") 
    @Test
    void testDivision() {
        assertEquals(5, calc.division(15, 3));
        assertEquals(Integer.MIN_VALUE, calc.division(15, 0));
        assertEquals(-5, calc.division(10, -2));
    }
   
  
    @DisplayName("Test Calculatrice.calcul()") 
    @Test
    void testCalcul() {
        assertEquals(calc.addition(0,2), calc.calcul(0, 2, "+"));
        assertEquals(calc.soustraction(5, 1), calc.calcul(5, 1, "-"));
        assertEquals(calc.multiplication(5, 2), calc.calcul(5, 2, "*"));
        assertEquals(calc.division(10, 2), calc.calcul(10, 2, "/"));
        assertThrows(IllegalArgumentException.class, () -> calc.calcul(0, 0, "m"));
    }
    
    
    @DisplayName("Test facto rec")
    @ParameterizedTest
    @CsvSource({"24, 4", "1, 1", "1, 0"})
    void testFactoRec(int expected, int facto) {
        assertEquals(expected, calc.factoRec(facto));
    }
    
    @DisplayName("Test facto iterative")
    @ParameterizedTest
    @CsvSource({"24, 4", "1, 1", "1, 0"})
    void testFactoIte(int expected, int facto) {
        assertEquals(expected, calc.factoIte(facto));
    }
      
    @DisplayName("Test Calculatrice.wait")
    @Test
    @Timeout(30)//faut toujours l'avoir supérieur
    void testWait() {
        boolean a = calc.waitTest(5000);
        assertEquals(a, true);
    }
    
    
    
   
}
